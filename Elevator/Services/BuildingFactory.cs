﻿using Elevator.Manager.Entities;

namespace Elevator.Manager.Services
{
    public static class BuildingFactory
    {
        public static Building GetBuilding(string address, int countOfFloors, int groundFloor, int liftCapacity)
        {
            var floors = FloorService.GetFloors(groundFloor, countOfFloors);
            //var floorQueues = FloorService.GetFloorQueues(floors);
            var lift = LiftFactory.GetLift(liftCapacity);
            return new Building
            {
                BuildingFloors = floors,
                Address = address,
                //FloorQueues = floorQueues,
                BuildingLift = lift
            };
        }
    }
}