﻿

using System.Collections.Generic;
using System.Linq;
using Elevator.Manager.Entities;

namespace Elevator.Manager.Services
{
    public class LiftService
    {
        private readonly Building _building;
        public List<Call> FloorStack { get; private set; } //queue of Calls to go
        private bool IsGoingUp { get; set; }
        
        public LiftService(Building building)
        {
            _building = building;
            FloorStack = new List<Call> { new Call(_building.BuildingFloors.First(), true) };
            FloorStack.First().IsActive = true;
            IsGoingUp = true;
        }

        public void LiftOperationUnit() //TD: cover with tests
        {
            _building.BuildingLift.Unload(ActiveFloor().Name); //unload - add string parameter
            ActiveFloor().LoadLift(_building, IsGoingUp);
            LiftMove();
        }

        public void LiftCall(string floorName)
        {
            var floor = _building.BuildingFloors.FirstOrDefault(x => x.Name == floorName);
            AddToStack(floor);
        }


        /// Private methods


        private void LiftMove()  // TD: cover with tests
        {
            var currentActiveFloor = ActiveFloor();

            if (IsGoingUp && FloorStack.Any(x => x.Floor.Id > currentActiveFloor.Id))
            {
                FloorStack.RemoveAll(x => x.Floor.Id == currentActiveFloor.Id);
                var nextFloor = FloorStack.First(c => c.Floor.Id > currentActiveFloor.Id);
                nextFloor.IsActive = true;
                return;
            }
            if (IsGoingUp && FloorStack.Any(x => x.Floor.Id < currentActiveFloor.Id))
            {
                IsGoingUp = false;
                FloorStack.RemoveAll(x => x.Floor.Id == currentActiveFloor.Id);
                var nextFloor = FloorStack.Last(c => c.Floor.Id < currentActiveFloor.Id);
                nextFloor.IsActive = true;
                return;
            }

            if (!IsGoingUp && FloorStack.Any(x => x.Floor.Id < currentActiveFloor.Id))
            {
                FloorStack.RemoveAll(x => x.Floor.Id == currentActiveFloor.Id);
                var nextFloor = FloorStack.Last(c => c.Floor.Id < currentActiveFloor.Id);
                nextFloor.IsActive = true;
                return;
            }
            
            if (!IsGoingUp && FloorStack.Any(x => x.Floor.Id > currentActiveFloor.Id))
            {
                IsGoingUp = true;
                FloorStack.RemoveAll(x => x.Floor.Id == currentActiveFloor.Id);
                var nextFloor = FloorStack.First(c => c.Floor.Id > currentActiveFloor.Id);
                nextFloor.IsActive = true;
                return;
            }
            
        }

        private Floor ActiveFloor()
        {
            return FloorStack.Single(x => x.IsActive).Floor;
        }


        


        private void AddToStack(Floor floor)
        {
            if (floor != null && FloorStack.All(c => c.Floor.Id != floor.Id))  
            {
                var currentActiveFloor = FloorStack.Single(x => x.IsActive);
                FloorStack.Add(new Call(floor, floor.Id >= currentActiveFloor.Floor.Id));
            }

            FloorStack = FloorStack.OrderBy(x => x.Floor.Id).ToList();
        }


        public void AddTestManToQueue(Man manToAdd, string floorName)  //for testing purposes
        {
            _building.BuildingFloors.First(x => x.Name == floorName).Queue.Add(manToAdd);
        }

        public Floor ActiveFloorTest()
        {
            return FloorStack.Single(x => x.IsActive).Floor;
        }

        public List<Man> FloorQueueByName(string floorName)
        {
            return this._building.BuildingFloors.First(x => x.Name == floorName).Queue;
        }

        /*private readonly Building _building;
        private List<int> RouteList = new List<int>();
        private int _minFloor, _topFloor;

        public LiftService(Building building)
        {
            _building = building;

        }

        public LiftService(int MinRoute, int MaxRoute)
        {
            _minFloor = MinRoute;
            _topFloor = MaxRoute;
        }

        public int GetNextFloor(Lift LiftInstance)
        {
            if (LiftInstance.LiftState == Lift.State.Up) return GetNextFloorUp(LiftInstance);
            else if (RouteList.Count() == 0)
            {
                LiftInstance.LiftState = Lift.State.Stop;
                return LiftInstance.CurrentLevel;
            }
            else if (RouteList.Count() == 1) return GetNextFloorOneCall(LiftInstance);
            else return GetNextFloorDown(LiftInstance); 

        }
        
        private int GetNextFloorOneCall(Lift LiftInstance)
        {
            return RouteList.FirstOrDefault();
        }
        
        private int GetNextFloorUp(Lift LiftInstance)
        {
            int temp;
            RouteList.OrderBy(k => k);
            if (RouteList.Max() <= LiftInstance.CurrentLevel)
            {
                LiftInstance.LiftState = Lift.State.Down;
                return GetNextFloor(LiftInstance);
            } 
            //for (int i = 0; i < RouteList.Count; i++)
            int i = 0;
            while (RouteList[i] <= LiftInstance.CurrentLevel && i < RouteList.Count-1)
            {
                temp = RouteList[i];
                i++;
                /*if (i == RouteList.Count - 1) return LiftInstance.CurrentLevel;
                else if (RouteList[i] > LiftInstance.CurrentLevel) return RouteList[i]; #1#
            }
            return RouteList[i];
            //if lift is loaded and routed
            //if any outer calls
            //if lift MaxLoad is on the edge - do not stop to load
        }

        private int GetNextFloorDown(Lift LiftInstance)
        {
            int temp;
            int nextFloor = 0;
            RouteList.OrderByDescending(k => k);

            if (RouteList.Min() >= LiftInstance.CurrentLevel)
            {
                LiftInstance.LiftState = Lift.State.Up;
                return GetNextFloor(LiftInstance);
            }

            int i = 0;
            while (RouteList[i] <= LiftInstance.CurrentLevel && i < RouteList.Count - 1)
            {
                i++;
            }
            return RouteList[i];

            return nextFloor;
        }

        public void LiftCall(int FloorNum)
        {
            if (FloorNum >= _minFloor && FloorNum <= _topFloor) 
            {
                RouteList.Add(FloorNum);
                RouteList.Sort();
            }
        }

        public List<int> ReturnRouteList()
        {
            return RouteList;
        }*/

    }
}
