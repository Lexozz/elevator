﻿using System;
using System.Collections.Generic;
using System.Linq;
using Elevator.Manager.Entities;
using Elevator.Manager.Logic;

namespace Elevator.Manager.Services
{
    public static class FloorService 
    {
        public static List<Floor> GetFloors(int groundFloorIndex, int maxFloor)
        {
            if (groundFloorIndex > maxFloor)
                throw new ArgumentException("Ground floor could not be higher than max");
            if (groundFloorIndex < 0)
                throw new ArgumentException("Ground floor could not be negative");

            var floorsBeforeGround = GetArray(groundFloorIndex, true);
            var outputList = new List<string>();
            outputList.AddRange(floorsBeforeGround);

            if (groundFloorIndex != maxFloor)
            {
                if (groundFloorIndex != 0)
                {groundFloorIndex -= 1;}
                outputList.AddRange(GetArray(maxFloor - groundFloorIndex, false));
            }
            return (outputList.Select((t, i) => new Floor(i, t))).ToList();
        }

        private static List<string> GetArray(int to, bool reversed)
        {
            var s = new List<string>();
            if (reversed && to == 0)
            {
                s.Add("0");
            }

            for (int i = (reversed ? 0 : 1); i < to; i++)
            {
                s.Add( reversed ? (-i).ToString() : i.ToString());
            }

            if(reversed)
                s.Reverse();
            return s;
        }

        public static Dictionary<string, List<Man>> GetFloorQueues(List<Floor> floors)
        {
            return floors.ToDictionary(floor => floor.Name, floor => new List<Man>());
        }


    }
}
