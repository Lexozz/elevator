﻿

using Elevator.Manager.Entities;

namespace Elevator.Manager.Services
{
    public static class ManFactory
    {
        public static Man GetMan(string targetLevel, int weight)
        {
            return new Man{TargetedLevel = targetLevel, Weight = weight};
        }

    }
}
