﻿
using System.Collections.Generic;
using Elevator.Manager.Entities;

namespace Elevator.Manager.Services
{
    public class LiftFactory
    {
        public static Lift GetLift(int capacity)
        {
            return new Lift
            {
                MaxCapacity = capacity
                
            };
        }

    }
}
