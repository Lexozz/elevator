﻿using System.Collections.Generic;
using System.Linq;

namespace Elevator.Manager.Entities
{
    public class Lift
    {
        internal Lift()
        {
            LiftQueue = new List<Man>();
        }

        public int MaxCapacity { get; set; }
        public List<Man> LiftQueue { get; set; }
        public int CurrentLoad { get; private set; }


        public List<Man> Load(List<Man> toLoadMan)  //modify for the weight limit
        {
            
            foreach (Man man in toLoadMan)
            {
                if (CurrentLoad + man.Weight <= MaxCapacity)
                {
                    LiftQueue.Add(man);
                    CurrentLoad += man.Weight;
                } 
            }
            
            return LiftQueue;
        }

        public void Unload(string currentFloor)
        {
            var listToUnload = LiftQueue.Where(c => c.TargetedLevel == currentFloor).ToList();
            LiftQueue = LiftQueue.Except(listToUnload).ToList();

            if (listToUnload.Any())
                CurrentLoad -= listToUnload.Sum(c => c.Weight);
        }

        

    }
}