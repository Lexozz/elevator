﻿using System.Collections.Generic;
using Elevator.Manager.Logic;

namespace Elevator.Manager.Entities
{
    public class Building
    {
        internal Building()
        {
            BuildingFloors = new List<Floor>();
            FloorQueues = new Dictionary<string, List<Man>>();
        }

        public string Address { set; get; }
        public List<Floor> BuildingFloors { get; set; }
        public Dictionary<string, List<Man>> FloorQueues { get; set; }
        public Lift BuildingLift { get; set; }
    }
}