﻿using System;
using Elevator.Manager.Entities;

namespace Elevator.Manager.Entities
{
    public class Man
    {
        public int Weight { get; set; }
        public string TargetedLevel { get; set; }

        internal Man()
        {
        }

    }
}
