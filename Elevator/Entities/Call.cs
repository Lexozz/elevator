﻿
namespace Elevator.Manager.Entities
{
    public class Call
    {
        public Floor Floor { get; set; }

        //private readonly Man _man;
        private readonly bool _isGoingUp;

        public bool IsActive { get; set; } 

        public Call(Floor floor, bool isGoingUp)
        {
            Floor = floor;
           // _man = man;
            _isGoingUp = isGoingUp;
            IsActive = false;
        }
    }
}
