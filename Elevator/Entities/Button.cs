﻿namespace Elevator.Manager.Entities
{
    public class Button
    {
        public bool State { get; set; }
        public int TargettedFloor { get; set; }

        public Button()
        {
            State = false;
        }

        public Button(int targettedFloor)
        {
            State = false;
            TargettedFloor = targettedFloor;
        }

        public void Click()
        {
            if (!State) State = true;
        }

        public void ButtonUncheck()
        {
            if (State) State = false;
        }
    }
}
