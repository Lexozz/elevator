﻿using System.Collections.Generic;
using System;
using System.Linq;

namespace Elevator.Manager.Entities
{
    public class Floor
    {
        public int Id { get; set; }
        public string Name { get; set; }
        public List<Man> Queue { get; set; }
        

        public Floor(int id, string name)
        {
            Id = id;
            Name = name;
            Queue = new List<Man>();
        }

        public Floor(int id, bool groundTopFloor)
        {
            Id = id;

        }

        public void LoadLift(Building building, bool isGoingUp)
        {
            List<Man> listToLoad = new List<Man>();
            foreach (Man man in Queue)
            {
                bool IsManGoingUp = Id < building.BuildingFloors.FirstOrDefault(x => x.Name == man.TargetedLevel).Id;
                if (IsManGoingUp == isGoingUp)
                {
                    listToLoad.Add(man);
                }
            }


            List<Man> loadedList = building.BuildingLift.Load(listToLoad);
            Queue = Queue.Except(loadedList).ToList();
        }

    }
}
