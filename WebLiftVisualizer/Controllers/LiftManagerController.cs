﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using System.Web.Services;
using Elevator.Manager.Entities;

namespace WebLiftVisualizer.Controllers
{
    public class LiftManagerController : Controller
    {
        public class JsonItem
        {
            public string Name { get; set; }
            public string Password { get; set; }
        }

        public class JsonData
        {
            public List<JsonItem> List { get; set; }

            public JsonData(List<JsonItem> list)
            {
                List= list;
            }

        
        }

        // GET: /LiftManager/
        [WebMethod]
        public ActionResult GetFloors(List<Floor> floorsList)
        {
//            var floors = new List<JsonItem>();
//            for (var i = 0; i < 10; i++)
//            {
//                floors.Add(new JsonItem
//                {
//                    Name = "Name_" + i,
//                    Password = "Password_" + i
//                });
//            }
//            JsonData jsData = new JsonData(floors);
            return View(floorsList);
        }

        private ActionResult View(List<Floor> floorsList)
        {
            throw new NotImplementedException();
        }
    }
}
