﻿using Elevator.Manager.Logic;
using Elevator.Manager.Entities;
using Elevator.Manager.Services;
using NUnit.Framework;
using System.Collections.Generic;
using System.Linq;

namespace Elevator.Test.LogicTest
{
    [TestFixture]
    public class LiftServiceTest
    {
        
        
        [Test]
        public void TestElevatorLoad()
        {
            Building building = BuildingFactory.GetBuilding("500 Blvd, New York", 10, 0, 500);
            LiftService liftService = new LiftService(building);
            List<Man> toLoad = new List<Man>();
            List<Man> ethalonList = new List<Man>();
            
            for (int i = 0; i < 5; i++)
            {
                Man tempMan = ManFactory.GetMan("5", 100);
                liftService.AddTestManToQueue(tempMan, "0");
                Assert.True(liftService.FloorQueueByName("0").Contains(tempMan), "Man is not in floor queue, i = " + i);
                //toLoad.Add(tempMan);
                ethalonList.Insert(0, tempMan);
                
            }

            liftService.LiftOperationUnit();
            
            Assert.That(building.BuildingLift.LiftQueue.Count, Is.EqualTo(5), "Lift was not fully loaded. Man count.");
            Assert.That(building.BuildingLift.CurrentLoad, Is.EqualTo(ethalonList.Sum(c => c.Weight)), "Lift current load value is not correct");
            Assert.That(liftService.FloorQueueByName("0").Count, Is.EqualTo(0), "Men were not removed from Floor Queue");
        }

        [Test]
        public void TestElevatorOverLoad()
        {
            Building building = BuildingFactory.GetBuilding("500 Blvd, New York", 10, 0, 500);
            LiftService liftService = new LiftService(building);
            List<Man> toLoad = new List<Man>();
            List<Man> ethalonList = new List<Man>();

            for (int i = 0; i < 7; i++)
            {
                Man tempMan = ManFactory.GetMan("5", 100);
                liftService.AddTestManToQueue(tempMan, "0");
                Assert.True(liftService.FloorQueueByName("0").Contains(tempMan), "Man is not in floor queue, i = " + i);
                //toLoad.Add(tempMan);
                ethalonList.Insert(0, tempMan);

            }

            liftService.LiftOperationUnit();

            Assert.That(building.BuildingLift.LiftQueue.Count, Is.EqualTo(5), "Lift was not fully loaded. Man count.");
            Assert.That(building.BuildingLift.CurrentLoad, Is.EqualTo(500), "Lift current load value is not correct");

            Assert.That(liftService.FloorQueueByName("0").Count, Is.EqualTo(2), "Due to capacity on floor should be still 2 men");
        }

        [Test]
        public void TestElevatorUnload()
        {
            Building building = BuildingFactory.GetBuilding("500 Blvd, New York", 10, 0, 500);
            LiftService liftService = new LiftService(building);
            List<Man> toLoad = new List<Man>();

            Man tempMan = ManFactory.GetMan("0", 100);
            toLoad.Add(tempMan);

            building.BuildingLift.LiftQueue = toLoad;
            //liftService.FloorStack.Add(new Call(building.BuildingFloors.First(x => x.Name == "5"), true));

            liftService.LiftOperationUnit();

            Assert.That(building.BuildingLift.LiftQueue.Count, Is.EqualTo(0), "Lift was not unloaded");
        }

        [Test]
        public void TestElevatorMove()
        {
            Building building = BuildingFactory.GetBuilding("500 Blvd, New York", 10, 0, 500);
            LiftService liftService = new LiftService(building);
            List<Man> toLoad = new List<Man>();

            Man tempMan = ManFactory.GetMan("5", 100);
            toLoad.Add(tempMan);
            
            building.BuildingLift.LiftQueue = toLoad;
            liftService.FloorStack.Add(new Call(building.BuildingFloors.First(x => x.Name == "5"), true));
            
            liftService.LiftOperationUnit();

            Assert.That(liftService.ActiveFloorTest(), Is.EqualTo(building.BuildingFloors.First(x => x.Name == "5")), "Lift was not moved");
        }

        [Test]
        public void TestElevatorMoveUpDown()
        {
            Building building = BuildingFactory.GetBuilding("500 Blvd, New York", 10, 0, 500);
            LiftService liftService = new LiftService(building);
            List<Man> toLoad = new List<Man>();

            Man tempMan = ManFactory.GetMan("5", 100);
            toLoad.Add(tempMan);

            building.BuildingLift.LiftQueue = toLoad;
            liftService.FloorStack.Add(new Call(building.BuildingFloors.First(x => x.Name == "5"), true));

            liftService.LiftOperationUnit();

            Assert.That(liftService.ActiveFloorTest(), Is.EqualTo(building.BuildingFloors.First(x => x.Name == "5")), "Lift was not moved");

            toLoad.Remove(tempMan);
            toLoad.Add(ManFactory.GetMan("3", 100));
            building.BuildingLift.LiftQueue = toLoad;
            liftService.FloorStack.Add(new Call(building.BuildingFloors.First(x => x.Name == "3"), false));

            liftService.LiftOperationUnit();

            Assert.That(liftService.ActiveFloorTest(), Is.EqualTo(building.BuildingFloors.First(x => x.Name == "3")), "Lift was not moved second time down");
        }

        [Test]
        public void TestElevatorMoveUpUp()
        {
            Building building = BuildingFactory.GetBuilding("500 Blvd, New York", 10, 0, 500);
            LiftService liftService = new LiftService(building);
            List<Man> toLoad = new List<Man>();

            Man tempMan = ManFactory.GetMan("5", 100);
            toLoad.Add(tempMan);

            building.BuildingLift.LiftQueue = toLoad;
            liftService.FloorStack.Add(new Call(building.BuildingFloors.First(x => x.Name == "5"), true));

            liftService.LiftOperationUnit();

            Assert.That(liftService.ActiveFloorTest(), Is.EqualTo(building.BuildingFloors.First(x => x.Name == "5")), "Lift was not moved");

            toLoad.Remove(tempMan);
            toLoad.Add(ManFactory.GetMan("7", 100));
            building.BuildingLift.LiftQueue = toLoad;
            liftService.FloorStack.Add(new Call(building.BuildingFloors.First(x => x.Name == "7"), true));

            liftService.LiftOperationUnit();

            Assert.That(liftService.ActiveFloorTest(), Is.EqualTo(building.BuildingFloors.First(x => x.Name == "7")), "Lift was not moved second time down");
        }

        [Test]
        public void TestElevatorMoveUpDownDown()
        {
            Building building = BuildingFactory.GetBuilding("500 Blvd, New York", 10, 0, 500);
            LiftService liftService = new LiftService(building);
            List<Man> toLoad = new List<Man>();

            Man tempMan = ManFactory.GetMan("5", 100);
            toLoad.Add(tempMan);

            building.BuildingLift.LiftQueue = toLoad;
            liftService.FloorStack.Add(new Call(building.BuildingFloors.First(x => x.Name == "5"), true));

            liftService.LiftOperationUnit();

            Assert.That(liftService.ActiveFloorTest(), Is.EqualTo(building.BuildingFloors.First(x => x.Name == "5")), "Lift was not moved");

            toLoad.Remove(tempMan);
            toLoad.Add(ManFactory.GetMan("3", 100));
            building.BuildingLift.LiftQueue = toLoad;
            liftService.FloorStack.Add(new Call(building.BuildingFloors.First(x => x.Name == "3"), false));

            liftService.LiftOperationUnit();

            Assert.That(liftService.ActiveFloorTest(), Is.EqualTo(building.BuildingFloors.First(x => x.Name == "3")), "Lift was not moved second time down");

            toLoad.Remove(tempMan);
            toLoad.Add(ManFactory.GetMan("2", 100));
            building.BuildingLift.LiftQueue = toLoad;
            liftService.FloorStack.Add(new Call(building.BuildingFloors.First(x => x.Name == "2"), false));

            liftService.LiftOperationUnit();

            Assert.That(liftService.ActiveFloorTest(), Is.EqualTo(building.BuildingFloors.First(x => x.Name == "2")), "Lift was not moved second time down");
        }

        [Test]
        public void TestElevatorMoveLastFloor()
        {
            Building building = BuildingFactory.GetBuilding("500 Blvd, New York", 10, 0, 500);
            LiftService liftService = new LiftService(building);
            List<Man> toLoad = new List<Man>();

            string lastFloorName = building.BuildingFloors.Last().Name;
            Man tempMan = ManFactory.GetMan(lastFloorName, 100);
            toLoad.Add(tempMan);

            building.BuildingLift.LiftQueue = toLoad;
            liftService.FloorStack.Add(new Call(building.BuildingFloors.First(x => x.Name == lastFloorName), true));

            liftService.LiftOperationUnit();

            Assert.That(liftService.ActiveFloorTest(), Is.EqualTo(building.BuildingFloors.Last()), "Lift was not moved");
        }

        [Test]
        public void TestElevatorMoveUpBottom()
        {
            Building building = BuildingFactory.GetBuilding("500 Blvd, New York", 10, 0, 500);
            LiftService liftService = new LiftService(building);
            List<Man> toLoad = new List<Man>();

            Man tempMan = ManFactory.GetMan("5", 100);
            toLoad.Add(tempMan);

            building.BuildingLift.LiftQueue = toLoad;
            liftService.FloorStack.Add(new Call(building.BuildingFloors.First(x => x.Name == "5"), true));

            liftService.LiftOperationUnit();

            Assert.That(liftService.ActiveFloorTest(), Is.EqualTo(building.BuildingFloors.First(x => x.Name == "5")), "Lift was not moved");

            toLoad.Remove(tempMan);
            toLoad.Add(ManFactory.GetMan("0", 100));
            building.BuildingLift.LiftQueue = toLoad;
            liftService.FloorStack.Add(new Call(building.BuildingFloors.First(x => x.Name == "0"), false));

            liftService.LiftOperationUnit();

            Assert.That(liftService.ActiveFloorTest(), Is.EqualTo(building.BuildingFloors.First(x => x.Name == "0")), "Lift was not moved second time down");
        }

        /*
        [Test]
        public void TestElevatorMaxLoad()
        {
            Building building = BuildingFactory.GetBuilding("500 Blvd, New York", 10, 0, 500);
            List<Man> toLoad = new List<Man>();
            for (int i = 0; i < 5; i++)
            {
                Man tempMan = ManFactory.GetMan("5", 110);
                toLoad.Add(tempMan);
                building.BuildingLift.Load(tempMan);
            }

            Assert.That(building.BuildingLift.LiftQueue.Count, Is.EqualTo(4), "Lift was not correctly loaded. Man count is not under max capacity.");
            Assert.That(building.BuildingLift.CurrentLoad, Is.EqualTo(440), "Lift current load value is not correct");
        }

        [Test]
        public void TestElevatorUnload()
        {
            Building building = BuildingFactory.GetBuilding("500 Blvd, New York", 10, 0, 500);
            List<Man> toLoad = new List<Man>();
            for (int i = 0; i < 4; i++)
            {
                Man tempMan = ManFactory.GetMan("5", 100);
                toLoad.Add(tempMan);
                building.BuildingLift.Load(tempMan);
            }
            Man tempMan2 = ManFactory.GetMan("4", 100);
            toLoad.Add(tempMan2);

            building.BuildingLift.Unload("4");

            Assert.That(building.BuildingLift.LiftQueue.Count, Is.EqualTo(4), "Lift was not correctly unloaded.");
            Assert.That(building.BuildingLift.CurrentLoad, Is.EqualTo(400), "Lift current load value is not correct");
            Assert.That(building.BuildingLift.LiftQueue.Contains(tempMan2), Is.False, "Man was not unloaded from lift");
        }

        [Test]
        public void TestElevatorFullUnload()
        {
            Building building = BuildingFactory.GetBuilding("500 Blvd, New York", 10, 0, 500);
            List<Man> toLoad = new List<Man>();
            for (int i = 0; i < 5; i++)
            {
                Man tempMan = ManFactory.GetMan("5", 100);
                toLoad.Add(tempMan);
                building.BuildingLift.Load(tempMan);
            }

            building.BuildingLift.Unload("5");

            Assert.That(building.BuildingLift.LiftQueue.Count, Is.EqualTo(0), "Lift was not correctly unloaded.");
            Assert.That(building.BuildingLift.CurrentLoad, Is.EqualTo(0), "Lift current load value is not correct");
        }*/
    }
}
