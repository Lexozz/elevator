﻿/*
using System.Collections.Generic;
using System.Linq;
using Elevator.Manager.Logic;
using Elevator.Manager.Entities;
using NUnit.Framework;

namespace Elevator.Test.LogicTest
{
     [TestFixture]
    class MoveServiceManagerTests
    {
         readonly LiftService _moveServiceInstance = new LiftService(-2, 10);

         [Test]
         public void RouteSingleLiftCallTest()
         {
             _moveServiceInstance.LiftCall(3);
             List<int> routeList = _moveServiceInstance.ReturnRouteList();
             Assert.True(routeList.Contains(3), "If failed - call from floor was not added");
         }

         [Test]
         public void RouteLiftCallSortTest()
         {
             _moveServiceInstance.LiftCall(3);
             _moveServiceInstance.LiftCall(-2);
             _moveServiceInstance.LiftCall(10);
             List<int> routeList = _moveServiceInstance.ReturnRouteList();
             List<int> equList = new List<int> {-2, 3, 10};
             //EquList.Sort();
             Assert.True(routeList.SequenceEqual(equList), "Sorting inside Route list is not correctly working.");
         }

         [Test]
         public void RouteNegativeLiftCallTest()
         {
             int value = 11;
             _moveServiceInstance.LiftCall(value);
             List<int> routeList = _moveServiceInstance.ReturnRouteList();
             Assert.False(routeList.Contains(value), "If failed - invalid positive call from floor was added");

             value = -3;
             _moveServiceInstance.LiftCall(value);
             routeList = _moveServiceInstance.ReturnRouteList();
             Assert.False(routeList.Contains(value), "If failed - invalid negative call from floor was added");
         }

         [Test]
         public void RouteNextFloorUpTest()
         {
             Lift elevatorTestInstance = new Lift(-2, 10);
             List<Man> toLoad = new List<Man>();
             Man tempMan = new Man(-2, 2); //man on same floor as lift and target floor "2";
             toLoad.Add(tempMan);

             elevatorTestInstance.Load(toLoad);

             int nextFloor = elevatorTestInstance.LiftMoveService.GetNextFloor(elevatorTestInstance);
             Assert.That(nextFloor == 2, "Next Floor is not returned correctly.");
             //Assert.That(elevatorTestInstance.LiftState == Lift.State.Stop,"Lift state is not STOP");
         }

         [Test]
         public void RouteNextFloorDownTest()
         {
             Lift elevatorTestInstance = new Lift(-2, 10);
             List<Man> toLoad = new List<Man>();
             Man tempMan = new Man(-2, 2); //man on same floor as lift and target floor "2";
             Man tempMan2 = new Man(-2, 4);
             toLoad.Add(tempMan);
             toLoad.Add(tempMan2);

             elevatorTestInstance.Load(toLoad);

             int nextFloor = elevatorTestInstance.LiftMoveService.GetNextFloor(elevatorTestInstance);
             Assert.That(nextFloor == 2, "Next Floor is not returned correctly.");
         }

     
    }

}
*/
