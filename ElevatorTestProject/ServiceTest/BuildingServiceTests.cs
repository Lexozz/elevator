﻿using Elevator.Manager.Services;
using Elevator.Manager.Entities;
using NUnit.Framework;

namespace Elevator.Test.ServiceTest
{
    [TestFixture]
    public class BuildingServiceTests
    {
        [Test]
        public void BasicBuildingFactoryTest()
        {
            Building building = BuildingFactory.GetBuilding("500 Blvd, New York", 10, 0, 500);

            Assert.That(building.Address, Is.EqualTo("500 Blvd, New York"), "Building address is not correct.");

            Assert.That(building.BuildingFloors[0].Name, Is.EqualTo("0"), "Ground floor is not zero");
            Assert.That(building.BuildingFloors[9].Name, Is.EqualTo("9"), "Top floor is not 9");
            Assert.That(building.FloorQueues.Count, Is.EqualTo(10),"FloorQueues count is not same as floors count");
            
            Assert.That(building.BuildingLift.CurrentLoad, Is.EqualTo(0), "Lift is not empty");
            Assert.That(building.BuildingLift.LiftQueue.Count, Is.EqualTo(0), "Lift queue is not empty");
            Assert.That(building.BuildingLift.MaxCapacity, Is.EqualTo(500), "Lift max capacity was not set");
        }
    }
}
