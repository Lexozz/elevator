﻿using System;
using System.Linq;
using Elevator.Manager.Services;
using NUnit.Framework;

namespace Elevator.Test.ServiceTest
{
    [TestFixture]
    public class FloorServiceTests
    {
        [Test]
        public void GetFloorsForPositive()
        {
            var floors = FloorService.GetFloors(2, 10);
            Assert.That(floors, Is.Not.Null);
            Assert.That(floors.Count, Is.EqualTo(10));
            Assert.That(floors.First().Id, Is.EqualTo(0));
            Assert.That(floors.First().Name, Is.EqualTo((-1).ToString()));
            Assert.That(floors.Last().Id, Is.EqualTo(9));
            Assert.That(floors.Last().Name, Is.EqualTo(8.ToString()));

            for (int i = 0; i < floors.Count; i ++)
            {
                Assert.That(floors[i].Id, Is.EqualTo(i));
                Assert.That(floors[i].Name, Is.EqualTo((i-1).ToString()));
            }

        }

        [Test]
        public void GetFloorsForGroundAndFirst()
        {
            var floors = FloorService.GetFloors(0, 10);
            Assert.That(floors, Is.Not.Null);
            Assert.That(floors.Count, Is.EqualTo(10));
            Assert.That(floors.First().Id, Is.EqualTo(0));
            Assert.That(floors.First().Name, Is.EqualTo((0).ToString()));
            Assert.That(floors.Last().Id, Is.EqualTo(9));
            Assert.That(floors.Last().Name, Is.EqualTo(9.ToString()));
        }

        [Test]
        public void GetFloorsForGroundAndLast()
        {
            var floors = FloorService.GetFloors(10, 10);
            Assert.That(floors, Is.Not.Null);
            Assert.That(floors.Count, Is.EqualTo(10));
            Assert.That(floors.First().Id, Is.EqualTo(0));
            Assert.That(floors.First().Name, Is.EqualTo((-9).ToString()));
            Assert.That(floors.Last().Id, Is.EqualTo(9));
            Assert.That(floors.Last().Name, Is.EqualTo(0.ToString()));
            
        }

        [Test]
        [ExpectedException(typeof(ArgumentException))]
        public void GetFloorsGroundBiggerThanMax()
        {
            FloorService.GetFloors(11, 10);
        }
        [Test]
        [ExpectedException(typeof(ArgumentException))]
        public void GetFloorsForNegative()
        {
            FloorService.GetFloors(-2, 10);
        }

    }
}
