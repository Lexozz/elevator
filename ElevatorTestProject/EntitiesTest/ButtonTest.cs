﻿using Elevator.Manager.Entities;
using NUnit.Framework;

namespace Elevator.Test.EntitiesTest
{
    [TestFixture]
    class ButtonTest
    {

        [Test]
        public void ButtonInitializationTest()
        {
            Button button = new Button();
            Assert.False(button.State);
        }
        
        [Test]
        public void ButtonFalseToTrueTest()
        {
            Button button = new Button();
            button.Click();
            Assert.True(button.State);
        }

        [Test]
        public void ButtonTrueToTrueTest()
        {
            Button button = new Button();
            button.Click();
            button.Click();
            Assert.True(button.State);
        }

        [Test]
        public void ButtonTrueToFalseTest()
        {
            Button button = new Button();
            button.Click();
            button.ButtonUncheck();
            Assert.False(button.State);
        }

        [Test]
        public void ButtonFalseToFalseTest()
        {
            Button button = new Button();
            Assert.False(button.State);
        }
    }
}
