﻿/*
using System.Collections.Generic;
using System.Linq;
using Elevator.Manager.Entities;
using Elevator.Manager.Logic;
using NUnit.Framework;

namespace Elevator.Test.EntitiesTest
{
    [TestFixture]



    class FloorQueueTest
    {
        FloorQueue floorQueue = new FloorQueue();
        List<Man> generatedList = new List<Man>() 
        { 
            new Man(5, 1), //0
            new Man(5, 7), //1
            new Man(5, 6), //2
            new Man(5, 2), //3
            new Man(5, 3), //4
            new Man(5, 9)  //5
        };
        
        [SetUp]
        public void FloorQueueInitialization()
        {

            for (int i = 0; i < 6; i++)
            {
                floorQueue.QueueAdding(generatedList[i]);
            }
                
        }                     // 9 6 7 1 2 3

        [Test]
        public void FloorQueueAddingTest()
        {
            List<Man> TestList = new List<Man>();
            TestList.Add(generatedList[5]);
            TestList.Add(generatedList[2]);
            TestList.Add(generatedList[1]);
            TestList.Add(generatedList[0]);
            TestList.Add(generatedList[3]);
            TestList.Add(generatedList[4]);
 
            Assert.AreEqual(floorQueue.Queue, TestList, "Queue's are not equal. QueueAdding function is not working properly.");
        }

        //Next tests are modifing predefind Floor Queue - do them last
        [Test]
        public void FloorTakeManUpTest()
        {
            Man takenGuy = floorQueue.TakeMan(true);
            Assert.AreEqual(takenGuy, generatedList[5], "Way Up man taking is not taking first guy");
            //Assert.False(floorQueue.Queue.Contains(generatedList[0]), "Man was not removed from list");
        }

        [Test]
        public void FloorTakeManDownTest()
        {
            int length = floorQueue.Queue.Count();
            Man takenGuy = floorQueue.TakeMan(false);
            //Assert.AreEqual(floorQueue.Queue.Count(), length - 1, "Not deleted");
            Assert.AreEqual(takenGuy, generatedList[4], "Way Down man taking is not taking last guy");
            //Assert.True(!floorQueue.Queue.Contains(generatedList[4]), "Man was not removed from list");
        }
    }
}
*/
