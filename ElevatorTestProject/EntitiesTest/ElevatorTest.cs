﻿/*

using System.Collections.Generic;
using Elevator.Manager.Entities;
using NUnit.Framework;

namespace Elevator.Test.EntitiesTest
{
     [TestFixture]
    class ElevatorTest
    {
         Lift elevatorTestInstance = new Lift(-2, 10);

         [Test]
         public void ElevatorMaxCapacitiesTest()
         {
             Assert.True(elevatorTestInstance.MaxCapacity == elevatorTestInstance.MaxWeightCapacity);
             Assert.True(elevatorTestInstance.MaxPeople == elevatorTestInstance.MaxPeopleCapacity);
         }

         [Test]
         public void TestElevatorLoad()
         {
             //Lift elevatorTestInstance = new Lift(-2, 10);
             List<Man> toLoad = new List<Man>();
             List<Man> ethalonList = new List<Man>();
             for (int i = 0; i < 5; i++)
             {
                 Man tempMan = new Man(100);
                 toLoad.Add(tempMan);
                 ethalonList.Insert(0, tempMan);
             }

             elevatorTestInstance.Load(toLoad);
             //Assert.AreEqual(elevatorTestInstance.CurrentLoad, ethalonList, "If failed - check MaxPeopleCapacity and MaxWeightCapacity, int this test 5 and 500 is used. If values are correct - Logic error");
             Assert.True(elevatorTestInstance.CurrentLoad[4] == ethalonList[0]);
             Assert.True(toLoad.Count == 0, "Was not fully loaded");
         }

         [Test]
         public void TestElevatorFullUnLoad()
         {
             //Elevator.Entities.Elevator elevatorTestInstance = new Elevator.Entities.Elevator(-2, 10);
             for (int i = 0; i < 5; i++)
             {
                 elevatorTestInstance.CurrentLoad.Add(new Man(100));
             }

             elevatorTestInstance.FullUnload();
             Assert.True(elevatorTestInstance.CurrentLoad.Count == 0, "Elevator wasn't unloaded");
         }

         [Test]
         public void TestIsLoaded()
         {
             elevatorTestInstance = new Lift(-2, 10);
             Assert.False(elevatorTestInstance.IsLoaded(), "IsLoaded is returning /loaded/ when Lift is empty");       
             List<Man> toLoad = new List<Man>();
            for (int i = 0; i < 5; i++)
             {
                 Man tempMan = new Man(100);
                 toLoad.Add(tempMan);
             }
             elevatorTestInstance.Load(toLoad);
             Assert.True(elevatorTestInstance.IsLoaded(), "IsLoaded is returning /not loaded/ when Lift is loaded");

         }

         [Test]
         public void TestRouteCreationOnLoad()
         {
             Lift elevatorTestInstance = new Lift(-2, 10);
             List<int> route = elevatorTestInstance.LiftMoveService.ReturnRouteList();
             Assert.That(route.Count == 0, "Route is not empty but should be.");
             List<Man> toLoad = new List<Man>();
             for (int i = 0; i < 5; i++)
             {
                 Man tempMan = new Man(100);
                 toLoad.Add(tempMan);
             }
             elevatorTestInstance.Load(toLoad);
             route = elevatorTestInstance.LiftMoveService.ReturnRouteList();
             Assert.That(route.Count != 0, "Route is empty but should not");
         }

         [Test]
         public void TestRouteValue()
         {
             Lift elevatorTestInstance = new Lift(-2, 10);
             List<Man> toLoad = new List<Man>();
             Man tempMan = new Man(70, 3, true);
             toLoad.Add(tempMan);
             elevatorTestInstance.Load(toLoad);
             List<int> route = elevatorTestInstance.LiftMoveService.ReturnRouteList();
             Assert.That(route.Contains(3) == true, "Route is not containing targeted floor number");
         }

         [Test]
         public void TestLiftMoveUp()
         {
             Lift elevatorTestInstance = new Lift(-2, 10);
             Assert.That(elevatorTestInstance.CurrentLevel == -2, "Lift current floor after generation is not -2.");
             List<Man> toLoad = new List<Man>();
             Man tempMan = new Man(-2, 2); //man on same floor as lift and target floor "2";
             //Man tempMan2 = new Man(-2, 2);
             toLoad.Add(tempMan);
             //toLoad.Add(tempMan2);

             elevatorTestInstance.Load(toLoad);
             elevatorTestInstance.LiftMove();
             Assert.That(elevatorTestInstance.CurrentLevel == 2, "Lift was not moved to the next floor.");
             Assert.That(elevatorTestInstance.CurrentLoad.Count == 0, "Lift was not unloaded.");
             //Assert.That(elevatorTestInstance.LiftState == Lift.State.Stop, "Lift state is not stopped");
         }

         [Test]
         public void TestLiftMoveUp2()
         {
             Lift elevatorTestInstance = new Lift(-2, 10);
             List<Man> toLoad = new List<Man>();
             Man tempMan = new Man(-2, 2); //man on same floor as lift and target floor "2";
             Man tempMan2 = new Man(-2, 4);
             toLoad.Add(tempMan);
             toLoad.Add(tempMan2);

             elevatorTestInstance.Load(toLoad);
             elevatorTestInstance.LiftMove();
             Assert.That(elevatorTestInstance.CurrentLevel == 2, "Lift was not moved to the next floor.");
             Assert.That(elevatorTestInstance.CurrentLoad.Count == 1, "Lift was not unloaded.");
             
             elevatorTestInstance.LiftMove();
             Assert.That(elevatorTestInstance.CurrentLevel == 4, "Lift was not moved to the next floor.");
             Assert.That(elevatorTestInstance.CurrentLoad.Count == 0, "Lift was not unloaded.");
             //Assert.That(elevatorTestInstance.LiftState == Lift.State.Stop, "Lift state is not stopped");
         }

         [Test]
         public void TestLiftMoveUpDown()
         {
             Lift elevatorTestInstance = new Lift(-2, 10);
             List<Man> toLoad = new List<Man>();
             Man tempMan = new Man(-2, 2); //man on same floor as lift and target floor "2";
             Man tempMan2 = new Man(2, 0);
             toLoad.Add(tempMan);

           //  elevatorTestInstance.Load(toLoad);
             //elevatorTestInstance.LiftMove();
             Assert.That(elevatorTestInstance.CurrentLevel == 2, "Lift was not moved to the next floor.");
             Assert.That(elevatorTestInstance.CurrentLoad.Count == 0, "Lift was not unloaded.");

             toLoad.Add(tempMan2);
             elevatorTestInstance.Load(toLoad);
             elevatorTestInstance.LiftMove();
             Assert.That(elevatorTestInstance.CurrentLevel == 0, "Lift was not moved to the next floor.");
             Assert.That(elevatorTestInstance.CurrentLoad.Count == 0, "Lift was not unloaded.");
             Assert.That(elevatorTestInstance.LiftState == Lift.State.Stop, "Lift state is not stopped");
         }

        
    }
}
*/
